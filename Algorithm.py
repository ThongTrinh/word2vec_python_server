import re
import numpy as np

import gensim
from nltk.corpus import gutenberg
from multiprocessing import Pool
from scipy import spatial
import sqlite3
import unicodedata
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE


def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except sqlite3.Error as e:
        print(e)
    return None

def select_tasks(conn):
    """
    Query all rows in the tasks table
    :param conn: the Connection object
    :return:
    """
    cur = conn.cursor()
    cur.execute("SELECT album, songname FROM music")
    rows = cur.fetchall()
    return rows

def select_task_by_priority(conn, priority):
    """
    Query tasks by priority
    :param conn: the Connection object
    :param priority:
    :return:
    """
    cur = conn.cursor()

    rows = cur.fetchall()

    for row in rows:
        print(type(row))

def preprocessing(rows):
    for index, row in enumerate(rows):
        rows[index] = [word.lower() for word in rows[index]]
        #rows[index] = str(rows[index]).strip()
        rows[index] = str(rows[index]).replace("\\n","")
        rows[index] = str(rows[index]).replace("  ", "")
        rows[index] = str(rows[index]).replace(", ", ",")
        rows[index] = str(rows[index]).replace(" ", "_")
        rows[index] = str(rows[index]).replace(",", ", ")
        #yield gensim.utils.simple_preprocess(row)

if __name__ == '__main__':
    ##################Database Connection###################
    # path = ("merged.db")
    # database = path
    # # create a database connection
    # conn = create_connection(database)
    # with conn:
    #     rows = select_tasks(conn)
    # preprocessing(rows)
    # # print(rows[0][2])
    # #newlist = (item.strip() if hasattr(item, 'strip') else item for item in rows)
    #
    # documents = list()
    # for row in rows:
    #     row = str(row).replace("\'", "")
    #     row = str(row).replace("[", "")
    #     row = str(row).replace("]", "")
    #     words = str(row).split(", ")
    #     documents.append(words)
    ##################Database Connection###################

    ##########IMPORTANT(build vocabulary and train model)######################
    # model = gensim.models.Word2Vec(
    #     documents,
    #     size=150,
    #     window=2,
    #     min_count=1,
    #     workers=10)
    # model.train(documents, total_examples=len(rows), epochs=10)
    # model.save("Word2VecModel.model")
    ##########IMPORTANT######################

    ##########Load existing model after training#########
    model = gensim.models.Word2Vec.load("Word2VecModel.model")
    ##########Load existing model#########

    ########File output Dictionary########
    # f = open("dictionary.txt", "wb")
    # for i in model.wv.vocab:
    #     f.write((i + "\n").encode('utf8'))
    # f.close()
    ########File output Dictionary########

    #########Measure similarity in group##########
    # print(model["ben_xua"])
    # w1 = ["chi_mai_xuong_cho"]
    # print(model.wv.most_similar(positive=w1))
    #########Measure similarity in group##########

    #########Measure similarity between 2 song##########
    # print(model.wv.similarity(w1="du_thuyen_tren_song_lam", w2="con_song_la"))
    # print(model.wv.similarity(w1="du_thuyen_tren_song_lam", w2="malala"))
    #########Measure similarity between 2 song##########

    #################SHOW GRAPH OF DATA####################
    # vocab = list(model.wv.vocab)
    # X = model[vocab[70:154]]
    # # pca = PCA(n_components=20, svd_solver='full', random_state=1001)
    # # X_pca = pca.fit_transform(X)
    # tsne = TSNE(perplexity=40, n_components=2, init='pca', n_iter=2500, random_state=23)
    # X_tsne = tsne.fit_transform(X)
    # df = pd.DataFrame(X_tsne, index=vocab[70:154], columns=['x', 'y'])
    # fig = plt.figure()
    # ax = fig.add_subplot(1, 1, 1)
    #
    # ax.scatter(df['x'], df['y'])
    # for word, pos in df.iterrows():
    #     ax.annotate(word, pos)
    #
    # plt.show()
    #################SHOW GRAPH OF DATA####################
