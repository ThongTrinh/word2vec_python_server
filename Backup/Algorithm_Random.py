import re
import numpy as np
import sys

import gensim
import random
from multiprocessing import Pool
from scipy import spatial
import sqlite3
import unicodedata
import pandas as pd
# import matplotlib.pyplot as plt
# from sklearn.decomposition import PCA
# from sklearn.manifold import TSNE


def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except sqlite3.Error as e:
        print(e)
    return None

def select_tasks(conn, option):
    """
    Query all rows in the tasks table
    :param conn: the Connection object
    :return:
    """
    cur = conn.cursor()
    if(option == 1):
        cur.execute("SELECT album, songname, artist, genre FROM music")
    else:
        cur.execute("SELECT album FROM music")
    rows = cur.fetchall()
    return rows

def select_task_by_priority(conn, priority):
    """
    Query tasks by priority
    :param conn: the Connection object
    :param priority:
    :return:
    """
    cur = conn.cursor()

    rows = cur.fetchall()

    for row in rows:
        print(type(row))

def preprocessing(rows, data_list):
    for index, row in enumerate(rows):
        rows[index] = [word.lower() for word in rows[index]]
        #rows[index] = str(rows[index]).strip()
        rows[index] = str(rows[index]).replace("\\n","")
        rows[index] = str(rows[index]).replace("  ", "")
        rows[index] = str(rows[index]).replace(", ", ",")
        rows[index] = str(rows[index]).replace(" ", "_")
        rows[index] = str(rows[index]).replace(",", ", ")
        #yield gensim.utils.simple_preprocess(row)

    for i, row in enumerate(rows):
        row = str(row).replace("\'", "")
        row = str(row).replace("[", "")
        row = str(row).replace("]", "")
        words = str(row).split(", ")
        data_list.append(words)

def train_word2vec(documents):
    print("Start training")
    model = gensim.models.Word2Vec(
        documents,
        size=50,
        window=1,
        min_count=2,
        workers=10)
    model.train(documents, total_examples=len(documents), epochs=10)
    model.save("OfficialModel.model")
    return  model

def generate_corpus(documents, data_list):
    #   album, songname, artist, gender
    #     nb_loop = 50
    #     for loop in range(0, nb_loop):
    #         print("Loop: ", loop)
    nb_of_docs = 1000000
    for i in range(0, nb_of_docs):
        if i % 100000 == 0:
            print("word: ", i)

        nb_of_word = random.randint(7, 15)
        random_record = random.randint(0, 323898)
        words = list()
        selected_word = data_list[random_record][1]
        # random_field = data_list[random_record].index(selected_word)
        words.append(selected_word)
        selected_element = list()
        try:
            for index, x in enumerate(data_list):
                if x[1] == selected_word:
                    selected_element.append(x)
        except IndexError:
            print("x: ", x, " index: ", index)
            continue
        #selected_element = [x for index, x in enumerate(data_list) if x[1] == selected_word print(x)]
        # del selected_element[1]
        if(len(selected_element) > 0):
            # for index, x in enumerate(selected_element):
            #     del selected_element[index][1]              #xoa cot songname
            for word_index in range(0, nb_of_word):
                if(len(selected_element) > 1):
                    index_of_selected_element = random.randint(0, (len(selected_element) - 1))
                else:
                    index_of_selected_element = 0
                selected_word = random.choice(selected_element[index_of_selected_element])
                words.append(selected_word)
            documents.append(words)

    print("Start writing file\n")
    with open('corpus.txt', 'w', encoding='utf-8') as f:
        for item in documents:
            sentence = ' '.join(item)
            f.write("%s\n" % sentence)

def read_corpus(documents):
    with open('corpus2_original.txt', encoding='utf-8') as f:
        for line in f:
            line = line.rstrip("\n")
            words = str(line).split(" ")
            documents.append(words)
if __name__ == '__main__':
    #################Database Connection###################
    # path = ("merged.db")
    # data_list = list()
    documents = list()
    # database = path
    # # create a database connection
    # conn = create_connection(database)
    # with conn:
    #     rows = select_tasks(conn, 1)
    # preprocessing(rows, data_list)
    #################Database Connection###################

    ###############Generate corpus###############
    #generate_corpus(documents, data_list)
    ###############Generate corpus###############

    #########IMPORTANT(build vocabulary and train model)######################
    # read_corpus(documents)
    # model = train_word2vec(documents)
    #########IMPORTANT######################

    ##########Load existing model after training#########
    model = gensim.models.Word2Vec.load("OfficialModel.model")
    # model.train(documents, total_examples=len(documents), epochs=10)
    # model.save("Word2VecModel_1_2_3.model")

    ##########Load existing model#########

    ##############TEST################
    # random_record = random.randint(0, 323898)
    # selected_word = data_list[7149][1]
    # print(selected_word)
    # selected_element = [x for i, x in enumerate(data_list) if i == 7149]
    # print(selected_element)
    #print(len(model.wv.vocab))
    ##############TEST################

    ######File output Dictionary########
    # f = open("dictionary.txt", "wb")
    # for i in model.wv.vocab:
    #     f.write((str(i) + "\n").encode('utf8'))
    # f.close()
    ######File output Dictionary########

    #########Measure similarity in group##########
    # print(model["ben_xua"])
    # input = input("nhap ten bai hay ca sy: ")
    print(model.wv.most_similar(positive='di_va_yeu'))
    #########Measure similarity in group##########

    #########Measure similarity between 2 song##########
    # print(model.wv.similarity(w1="open_your_eyes", w2="endless_quest"))
    # print(model.wv.similarity(w1="open_your_eyes", w2="hurts_like_hell_(feat._fleurie)"))
    #########Measure similarity between 2 song##########

    #################SHOW GRAPH OF DATA####################
    # vocab = list(model.wv.vocab)
    # X = model[vocab[70:154]]
    # # pca = PCA(n_components=20, svd_solver='full', random_state=1001)
    # # X_pca = pca.fit_transform(X)
    # tsne = TSNE(perplexity=40, n_components=2, init='pca', n_iter=2500, random_state=23)
    # X_tsne = tsne.fit_transform(X)
    # df = pd.DataFrame(X_tsne, index=vocab[70:154], columns=['x', 'y'])
    # fig = plt.figure()
    # ax = fig.add_subplot(1, 1, 1)
    #
    # ax.scatter(df['x'], df['y'])
    # for word, pos in df.iterrows():
    #     ax.annotate(word, pos)
    #
    # plt.show()
    #################SHOW GRAPH OF DATA####################
