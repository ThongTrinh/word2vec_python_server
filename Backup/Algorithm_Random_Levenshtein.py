import re
import sys
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.neighbors import NearestNeighbors
import gensim
import random
from multiprocessing import Pool
from scipy import spatial
import sqlite3
import unicodedata
import pandas as pd
# import matplotlib.pyplot as plt
# from sklearn.decomposition import PCA
# from sklearn.manifold import TSNE


def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except sqlite3.Error as e:
        print(e)
    return None

def select_tasks(conn, option):
    """
    Query all rows in the tasks table
    :param conn: the Connection object
    :return:
    """
    cur = conn.cursor()
    if(option == 1):
        cur.execute("SELECT album, songname, artist, genre FROM music")
    else:
        cur.execute("SELECT album FROM music")
    rows = cur.fetchall()
    return rows

def select_task_by_priority(conn, priority):
    """
    Query tasks by priority
    :param conn: the Connection object
    :param priority:
    :return:
    """
    cur = conn.cursor()

    rows = cur.fetchall()

    for row in rows:
        print(type(row))

def preprocessing(rows, data_list):
    for index, row in enumerate(rows):
        rows[index] = [word.lower() for word in rows[index]]
        #rows[index] = str(rows[index]).strip()
        rows[index] = str(rows[index]).replace("\\n","")
        rows[index] = str(rows[index]).replace("  ", "")
        rows[index] = str(rows[index]).replace(", ", ",")
        rows[index] = str(rows[index]).replace(" ", "_")
        rows[index] = str(rows[index]).replace(",", ", ")
        #yield gensim.utils.simple_preprocess(row)

    for i, row in enumerate(rows):
        row = str(row).replace("\'", "")
        row = str(row).replace("[", "")
        row = str(row).replace("]", "")
        words = str(row).split(", ")
        data_list.append(words)

def train_word2vec(documents):
    print("Start training")
    model = gensim.models.Word2Vec(
        documents,
        size=50,
        window=1,
        min_count=2,
        workers=5)
    model.train(documents, total_examples=len(documents), epochs=10)
    model.save("OfficialModel_ver2.model")
    return  model

def generate_corpus(documents, data_list):
    #   album, songname, artist, gender
    #     nb_loop = 50
    #     for loop in range(0, nb_loop):
    #         print("Loop: ", loop)
    nb_of_docs = 1000000
    for i in range(0, nb_of_docs):
        if i % 100000 == 0:
            print("word: ", i)

        nb_of_word = random.randint(7, 15)
        random_record = random.randint(0, 323898)
        words = list()
        selected_word = data_list[random_record][1]
        # random_field = data_list[random_record].index(selected_word)
        words.append(selected_word)
        selected_element = list()
        try:
            for index, x in enumerate(data_list):
                if x[1] == selected_word:
                    selected_element.append(x)
        except IndexError:
            print("x: ", x, " index: ", index)
            continue
        #selected_element = [x for index, x in enumerate(data_list) if x[1] == selected_word print(x)]
        # del selected_element[1]
        if(len(selected_element) > 0):
            # for index, x in enumerate(selected_element):
            #     del selected_element[index][1]              #xoa cot songname
            for word_index in range(0, nb_of_word):
                if(len(selected_element) > 1):
                    index_of_selected_element = random.randint(0, (len(selected_element) - 1))
                else:
                    index_of_selected_element = 0
                selected_word = random.choice(selected_element[index_of_selected_element])
                words.append(selected_word)
            documents.append(words)

    print("Start writing file\n")
    with open('corpus.txt', 'w', encoding='utf-8') as f:
        for item in documents:
            sentence = ' '.join(item)
            f.write("%s\n" % sentence)

def read_corpus(documents):
    with open('corpus2_original.txt', encoding='utf-8') as f:
        for line in f:
            line = line.rstrip("\n")
            words = str(line).split(" ")
            documents.append(words)

def levenshtein(s1, s2):
    if len(s1) < len(s2):
        return levenshtein(s2, s1)

    # len(s1) >= len(s2)
    if len(s2) == 0:
        return len(s1)

    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[
                             j + 1] + 1  # j+1 instead of j since previous_row and current_row are one character longer
            deletions = current_row[j] + 1  # than s2
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row

    return previous_row[-1]

def get_infor(my_song, conn):
    for i in my_song:
        cur = conn.cursor()
        songname = str(i[0]).replace('_', ' ').capitalize()
        cur.execute("SELECT album, songname, artist, genre FROM music where lower(songname) like ?", ('%'+songname+'%',))
        rows = cur.fetchall()
    return rows

def get_random_song(history, model):
    pass
if __name__ == '__main__':
    #################Database Connection###################
    # path = ("merged.db")
    # data_list = list()
    documents = list()
    # database = path
    # # create a database connection
    # conn = create_connection(database)
    # with conn:
    #     rows = select_tasks(conn, 1)
    # preprocessing(rows, data_list)
    #################Database Connection###################

    ###############Generate corpus###############
    #generate_corpus(documents, data_list)
    ###############Generate corpus###############

    #########IMPORTANT(build vocabulary and train model from corpus)######################
    # read_corpus(documents)
    # model = train_word2vec(documents)
    #########IMPORTANT######################

    ##########Load existing model after training#########
    model = gensim.models.Word2Vec.load("OfficialModel.model")
    ##########Load existing model#########

    ###########User input###########
    input = input("Input your song/artist/album/genre: ")
    input = input.lower()
    input = input.replace(" ", "_")
    # my_song = model.most_similar(positive=input)
    # print(my_song)
    ###########User input###########

    ###################Get full information of searched songs###########################
    # rows = get_infor(my_song, conn)
    ###################Get full information of searched songs###########################

    #################Levenshtein Distance####################
    distance = len(input)/2
    selected_word = list()
    for word in model.wv.vocab:
        temp_distance = levenshtein(input, word)
        if(temp_distance < distance):
            selected_word.append(word)
    for i in selected_word:
        print(i, "\n")
    #################Levenshtein Distance####################
