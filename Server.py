import sqlite3
from flask import Flask
from flask_restful import reqparse, abort, Api, Resource
import OfficialSource
import sys
app = Flask(__name__)
api = Api(app)

def execute_query(query):
    conn = sqlite3.connect('merged1.db')
    c = conn.cursor()
    c.execute(query)
    result = c.fetchall()
    conn.close()
    return result

parser = reqparse.RequestParser()
parser.add_argument('name', location='args')
parser.add_argument('selected_song', location='args')
model = OfficialSource.load_model()

# Search for songs, input can be anything from song name, artist to genre
# Perform full text search on Sqllite
class SongSearch(Resource):
    def get(self):
        args = parser.parse_args()
        query = f"select * from SearchTable where SearchTable match '{args['name']}';"
        return execute_query(query)

def recommend_song(song):
    requested_song = song.lower()
    requested_song = requested_song.replace(" ", "_")
    my_song = model.most_similar(positive=requested_song, topn=30)
    rows = list()
    for i in my_song:
        songname = str(i[0]).replace('_', ' ').capitalize()
        # print(songname, file=sys.stdout)
        query = f"SELECT album, songname, artist, genre FROM SearchTable where songname match '{songname}' limit 3"
        row = execute_query(query)
        if len(row) == 0:
            continue
        for r in row:
            rows.append(r)
    return rows

class SongRecommendation(Resource):
    def get(self):
        args = parser.parse_args()
        print(type(args['selected_song']), file=sys.stdout)
        list_song = recommend_song(args['selected_song'])
        return list_song

api.add_resource(SongSearch, '/search')
api.add_resource(SongRecommendation, '/')

if __name__ == '__main__':
    app.run(debug=True)