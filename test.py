import random
import numpy as np

#
# random_selection = random.choice(a[0])
# print(random_selection)
# print(a[0].index(random_selection))
# selected_element = [x for i, x in enumerate(a) if x[0] == 'a']
# del selected_element[0][0]
# print(selected_element)
# print(selected_element[0])
# graph = {}
# graph['bai_hat'] = set()
# graph['bai_hat'].add('tac_gia')
# graph['bai_hat'].add('ca_si')
#
# graph['ca_si'] = set()
# graph['ca_si'].add('bai_hat')
# graph['ca_si'].add('tac_gia')
# word = list()
# print(type(word))
# print(random.choice(list(graph['bai_hat'])))

# def levenshtein(seq1, seq2):
#     size_x = len(seq1) + 1
#     size_y = len(seq2) + 1
#     matrix = np.zeros ((size_x, size_y))
#     for x in range(size_x):
#         matrix [x, 0] = x
#     for y in range(size_y):
#         matrix [0, y] = y
#
#     for x in range(1, size_x):
#         for y in range(1, size_y):
#             if seq1[x-1] == seq2[y-1]:
#                 matrix [x,y] = min(
#                     matrix[x-1, y] + 1,
#                     matrix[x-1, y-1],
#                     matrix[x, y-1] + 1
#                 )
#             else:
#                 matrix [x,y] = min(
#                     matrix[x-1,y] + 1,
#                     matrix[x-1,y-1] + 1,
#                     matrix[x,y-1] + 1
#                 )
#     print (matrix)
#     return (matrix[size_x - 1, size_y - 1])


# def levenshtein(s1, s2):
#     if len(s1) < len(s2):
#         return levenshtein(s2, s1)
#
#     # len(s1) >= len(s2)
#     if len(s2) == 0:
#         return len(s1)
#
#     previous_row = range(len(s2) + 1)
#     for i, c1 in enumerate(s1):
#         current_row = [i + 1]
#         for j, c2 in enumerate(s2):
#             insertions = previous_row[
#                              j + 1] + 1  # j+1 instead of j since previous_row and current_row are one character longer
#             deletions = current_row[j] + 1  # than s2
#             substitutions = previous_row[j] + (c1 != c2)
#             current_row.append(min(insertions, deletions, substitutions))
#         previous_row = current_row
#
#     return previous_row[-1]

def levenshtein(source, target):
    if len(source) < len(target):
        return levenshtein(target, source)

    # So now we have len(source) >= len(target).
    if len(target) == 0:
        return len(source)

    # We call tuple() to force strings to be used as sequences
    # ('c', 'a', 't', 's') - numpy uses them as values by default.
    source = np.array(tuple(source))
    target = np.array(tuple(target))

    # We use a dynamic programming algorithm, but with the
    # added optimization that we only need the last two rows
    # of the matrix.
    previous_row = np.arange(target.size + 1)
    for s in source:
        # Insertion (target grows longer than source):
        current_row = previous_row + 1

        # Substitution or matching:
        # Target and source items are aligned, and either
        # are different (cost of 1), or are the same (cost of 0).
        current_row[1:] = np.minimum(
                current_row[1:],
                np.add(previous_row[:-1], target != s))

        # Deletion (target grows shorter than source):
        current_row[1:] = np.minimum(
                current_row[1:],
                current_row[0:-1] + 1)

        previous_row = current_row

    return previous_row[-1]

if __name__ == '__main__':
    matrix = levenshtein("son tung", "son tùng")
    print(matrix)